﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	private float verti;
	private float hori;
	private Vector3 offsetZ;
	private Vector3 offSetY;

	public Transform target;
	public float timeToReach;
	public Vector3 velocity;
	public Transform cam;

	// Update is called once per frame
	void FixedUpdate ()
	{
		verti = Input.GetAxis ("Vertical");
		hori = Input.GetAxis ("Horizontal");
		//check if target exists before following
		if (this.target != null)
		{
			offsetZ = (2 * -target.forward);
			offSetY = (1 * target.up);

			transform.position = Vector3.SmoothDamp (transform.position, target.position + offsetZ + offSetY, ref velocity, timeToReach);
			transform.rotation = target.rotation;
		}
	}

	void Update()
	{

		if (target == null || target.GetComponent<Plane>().isDead)
		{
			StartCoroutine(ExplosionShake());
		}
		else
		{
			bool isTargetBoosting = target.GetComponent<Plane>().isBoosting;
			if (isTargetBoosting)
			{
				BoostingShake();
			}
		}


	}

	IEnumerator ExplosionShake()
	{
		float offset = 1f;

		while(offset > 0)
		{
			cam.localPosition = Random.insideUnitSphere * offset;

			offset -= 0.7f * Time.deltaTime;

			yield return null;
		}
		
	}

	void BoostingShake()
	{
		float offset = 0.1f;
		cam.localPosition = Random.insideUnitSphere * offset;
		
	}
}
