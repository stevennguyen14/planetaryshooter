﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour {

	public GameObject explosion;
	public float blastRadius;
	public float explosionForce;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, blastRadius);
	}

	void OnCollisionEnter()
	{
		Instantiate(explosion, transform.position, Quaternion.identity);

		Collider [] colliders = Physics.OverlapSphere(transform.position, blastRadius);

		foreach(Collider nearbyObject in colliders)
		{
			Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
			if(rb != null && nearbyObject.CompareTag("Asset"))
			{
				rb.AddExplosionForce(explosionForce, transform.position, blastRadius);
			}
			else if (nearbyObject.CompareTag("Enemy"))
			{
				nearbyObject.gameObject.GetComponent<Turret>().health -= 20f;
				nearbyObject.gameObject.GetComponent<Turret>().StartCoroutine("FlashDamage");
			}

		}

		Destroy(gameObject);

	}
}
