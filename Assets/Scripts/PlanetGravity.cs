﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGravity : MonoBehaviour {

	public float gravityRange;
	public float gravity;

	private GameObject[] assets;

	// Use this for initialization
	void Start () {
		if(assets == null)
		{
			assets = GameObject.FindGameObjectsWithTag("Asset");
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		foreach(GameObject randomAssets in assets)
		{
			//Get distances of objects from planet
			float assetDistance = Vector3.Distance(randomAssets.transform.position, transform.position);
			//check if asset is within gravity range
			if (assetDistance <= gravityRange)
			{
				Rigidbody rb = randomAssets.GetComponent<Rigidbody>();
				if (rb != null)
				{
					Vector3 forceDirection = (randomAssets.transform.position - transform.position).normalized;
					rb.AddForce(-forceDirection * gravity);
				}
			}
			
		}
		
		
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position, gravityRange);
	}
}
