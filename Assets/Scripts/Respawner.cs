﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawner : MonoBehaviour {

	public GameObject player;

	private bool isDead;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(player != null)
		{
			if (player.GetComponent<Plane>().isDead)
			{
				StartCoroutine(Respawn());
			}
		}

	}

	IEnumerator Respawn()
	{
		player.SetActive(false);
		yield return new WaitForSeconds(2);
		if(player != null)
		{
			player.SetActive(true);
			player.transform.position = transform.position;
			player.transform.rotation = transform.rotation;
			player.GetComponent<Plane>().isDead = false;
		}

	}
}
