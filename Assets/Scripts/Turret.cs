﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Turret : MonoBehaviour {

	//public
	public Transform target;
	public float range = 100f;
	public Transform head;
	public GameObject bullet;
	public float bulletSpeed;
	public float fireRate;
	public GameObject explosion;
	public float explosionForce;
	public float blastRadius;
	public GameObject camera;

	//ui
	public float health = 100f;
	public Slider hpBar;
	public Image hpImg;
	public Transform canvas;

	//flash color when hit
	public Renderer turretMesh;
	public Renderer turretHeadMesh;
	public float flashDelay;
	public Color normalColor;
	public Color flashColor;

	//private
	private float nextFire;
	private float targetDistance;

	// Use this for initialization
	void Start () {
		hpImg.color = Color.green;
		hpBar.value = health;
		camera = GameObject.FindGameObjectWithTag("CameraBoom");
	}


	// Update is called once per frame
	void Update() {
		if (target == null)
		{
			return;
		}

		head.LookAt(target.position + target.forward * 2);
		Debug.DrawLine(head.position, target.position);

		targetDistance = Vector3.Distance(target.position, transform.position);

		if(targetDistance < range)
		{
			if (Time.time > nextFire)
			{
				nextFire = Time.time + fireRate;
				GameObject instantiatedBullet = Instantiate(bullet, head.transform.position, transform.rotation);
				instantiatedBullet.GetComponent<Rigidbody>().velocity = ((target.position + target.forward * 15) - head.transform.position).normalized * bulletSpeed;
			}
		}

	}

	void FixedUpdate()
	{
		//canvas.rotation = Camera.main.transform.rotation;
		
	}

	public void TurretUIUpdate()
	{
		if(health <= 75)
		{
			hpImg.color = Color.yellow;
		}
		if(health <= 25)
		{
			hpImg.color = Color.red;
		}

		hpBar.value = health;
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, range);
	}

	IEnumerator FlashDamage()
	{
		var headRenderer = turretHeadMesh;
		var renderer = turretMesh;
		//check if renderrs aren't null
		if (renderer != null && headRenderer != null)
		{
			//flash first color
			headRenderer.material.color = flashColor;
			renderer.material.color = flashColor;
			//delay
			yield return new WaitForSeconds(flashDelay);
			//flash back to normal color
			headRenderer.material.color = normalColor;
			renderer.material.color = normalColor;
			yield return new WaitForSeconds(flashDelay);
		}
	}

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.CompareTag("Bullet"))
		{
			health-= 2;
			TurretUIUpdate();
			StartCoroutine(FlashDamage());
		}
		else if (col.gameObject.CompareTag("Rocket"))
		{
			health -= 20f;
			TurretUIUpdate();
			StartCoroutine(FlashDamage());
		}

		if (health <= 0)
		{

			Destroy(Instantiate(explosion, transform.position, Quaternion.identity), 3);

			GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>().AddScore();

			Collider[] colliders = Physics.OverlapSphere(transform.position, blastRadius);

			foreach (Collider nearbyObject in colliders)
			{
				Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
				if (rb != null && nearbyObject.CompareTag("Asset"))
				{
					rb.AddExplosionForce(explosionForce, transform.position, blastRadius);
				}

			}

			Destroy(gameObject);

			camera.GetComponent<CameraFollow>().StartCoroutine("ExplosionShake");
		}
	}
}
