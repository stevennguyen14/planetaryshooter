﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketPowerup : MonoBehaviour {

	public GameObject particleEffect;

	void OnTriggerEnter(Collider col)
	{
		if (col.CompareTag("Player"))
		{
			col.GetComponent<Shooting>().singleShoot = false;
			col.GetComponent<Shooting>().rocketShoot = true;
			col.GetComponent<Shooting>().tripleShoot = false;
			Destroy(Instantiate(particleEffect, transform.position, transform.rotation), 1);
			Destroy(gameObject);
		}
	}


}
