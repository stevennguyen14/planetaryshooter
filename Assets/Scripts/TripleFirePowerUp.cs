﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripleFirePowerUp : MonoBehaviour {

	public GameObject particleEffect;

	void OnTriggerEnter(Collider col)
	{
		if(col.CompareTag("Player"))
		{
			col.GetComponent<Shooting>().singleShoot = false;
			col.GetComponent<Shooting>().rocketShoot = false;
			col.GetComponent<Shooting>().tripleShoot = true;
			Destroy(Instantiate(particleEffect, transform.position, transform.rotation), 1);
			Destroy(gameObject);
		}
	}

}
