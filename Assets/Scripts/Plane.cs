﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : MonoBehaviour {

	private float hori, verti, boost;
	private Rigidbody rb;
	private float defaultRotateSpeed;
	private float defaultUpDownSpeed;
	private float defaultMaximumSpeed;

	public bool isDead;
	public float maximumSpeed;
	public float rotateSpeed;
	public float upDownSpeed;
	public float boostedUpDownSpeed;
	public float boostedMaximumSpeed;
	public float boostedRotateSpeed;
	public bool isBoosting;
	public GameObject skySphere;
	public GameObject boosterParticle;
	public GameObject explosion;
	public Texture2D cursorTexture;
	public CursorMode cursorMode = CursorMode.Auto;
	public Vector2 hotSpot = Vector2.zero;

	void Start()
	{
		rb = GetComponent<Rigidbody> ();
		//when the game starts, set the booster particle to false (only need to be enabled when player press shift)
		boosterParticle.SetActive (false);
		Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
		defaultRotateSpeed = rotateSpeed;
		defaultUpDownSpeed = upDownSpeed;
		defaultMaximumSpeed = maximumSpeed;
		isDead = false;
	}
		
	void FixedUpdate () 
	{
		hori = Input.GetAxis ("Horizontal");
		verti = Input.GetAxis ("Vertical");
		boost = Input.GetAxis ("Boost");

		//have ship constantly flying forward, times this by the maximumSpeed 
		rb.velocity = (transform.forward * maximumSpeed);
		//when horizontal axis is pressed, ship rotates left or right
		transform.Rotate(Vector3.forward * -hori * rotateSpeed * Time.deltaTime);
		//when vertical axis is pressed, ship rotates up and down
		transform.Rotate (Vector3.right * verti * upDownSpeed * Time.deltaTime);
		skySphere.transform.position = transform.position;

		//if player press shift then change the speed and enable the particle effect
		if (boost > 0)
		{
			boosterParticle.SetActive (true);
			isBoosting = true;
			upDownSpeed = boostedUpDownSpeed;
			rotateSpeed = boostedRotateSpeed;
			if (maximumSpeed < boostedMaximumSpeed)
			{
				maximumSpeed += 0.5f;
			}
		}
		//when player stops holding shift, then revert back to normal speed and disable the particle
		else if (maximumSpeed > defaultMaximumSpeed)
		{
			maximumSpeed -= 0.5f;
			upDownSpeed = defaultUpDownSpeed;
			rotateSpeed = defaultRotateSpeed;
			boosterParticle.SetActive (false);
			isBoosting = false;
		}

	}

	void Update()
	{
		//if player leaves the area, then teleport back to the start
		if (transform.position.x > 280 || transform.position.x < -280 || transform.position.y > 280 || transform.position.y < -280 || transform.position.z > 280 || transform.position.z < -280) 
		{
			transform.position = new Vector3 (0, 0, -250);
			transform.rotation = Quaternion.Euler (0, 0, 0);
		}

	}

	//if player colliders with anything, then explode and die
	void OnCollisionEnter(Collision col)
	{
		Destroy(Instantiate(explosion, transform.position, Quaternion.identity), 3);
		

		GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>().DeductScore();
		GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>().DeductLife();

		isDead = true;

		if(GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>().life <= 0)
		{
			Destroy(gameObject);
		}
	}
}
