﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public Canvas hudCanvas;
	public Text scoreUI;
	public Text lifeUI;
	public Text gameOverScoreUI;
	public CanvasGroup gameOverCanvasGroup;
	public CanvasGroup scoreCanvasGroup;
	public float gameOverFadeTime;

	public int score;
	public int life;

	// Use this for initialization
	void Start () {

		score = 0;
		UpdateUI();
		life = 3;
		gameOverCanvasGroup.alpha = 0;
		scoreCanvasGroup.alpha = 0;
	}

	void Update()
	{

	}

	public void AddScore()
	{
		score += 100;
		UpdateUI();
	}

	public void DeductScore()
	{
		score -= 200;
		UpdateUI();
	}
	
	void UpdateUI () {
		scoreUI.text = score.ToString();
		gameOverScoreUI.text = "SCORE " + score.ToString();
	}


	public void DeductLife()
	{
		life--;
		lifeUI.text = "x" + life;
		if(life <= 0)
		{
			hudCanvas.gameObject.SetActive(false);
			//fade in gameover text
			StartCoroutine(fadeGameOver());
		}
	}

	//fade in game over text
	IEnumerator fadeGameOver()
	{
		//GameOver text fade in
		for(float i = 0.01f; i < gameOverFadeTime; i += 0.4f * Time.deltaTime)
		{
			gameOverCanvasGroup.alpha = i;
			yield return null;
		}
		//Score text fade in
		for (float i = 0.01f; i < gameOverFadeTime; i += 0.5f * Time.deltaTime)
		{
			scoreCanvasGroup.alpha = i;
			yield return null;
		}
	}
}

