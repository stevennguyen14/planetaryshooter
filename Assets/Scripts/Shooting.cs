﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour 
{
	public Camera camera;
	public GameObject bullet;
	public GameObject rocket;
	public float fireRate;
	public float rocketRate;
	public Transform nozzle;
	public float bulletSpeed, rocketSpeed;
	public bool singleShoot, tripleShoot, rocketShoot;
	public float countDown;

	private int mask;
	private float nextFire, nextRocket;
	private bool isPoweredUp;
	private float currentCountDown;
	private float poweredFireRate;
	private float defaultFireRate;


	void Start()
	{
		mask += 1 << 8;
		mask += 1 << 10;
		singleShoot = true;
		currentCountDown = countDown;
		defaultFireRate = fireRate;
		poweredFireRate = 0.05f;
	}
	
	// Update is called once per frame
	void Update () 
	{
			
		RaycastHit hit;
		Ray ray = camera.ScreenPointToRay (Input.mousePosition);
		if (Physics.Raycast (ray, out hit,1000f, mask))
		{
			//print (hit.collider.ToString());
			Debug.DrawRay (nozzle.transform.position, hit.point, Color.green);

			if (Input.GetKey(KeyCode.Mouse0))
			{
				if (singleShoot && Time.time > nextFire)
				{
					nextFire = Time.time + fireRate;
					Shoot(hit);
				}
				else if (tripleShoot && Time.time > nextFire)
				{
					nextFire = Time.time + fireRate;
					isPoweredUp = true;
					TripleShoot(hit);
				}
				else if (rocketShoot && Time.time > nextRocket)
				{
					nextRocket = Time.time + rocketRate;
					isPoweredUp = true;
					RocketShoot(hit);
				}
			}

		}

		if (isPoweredUp)
		{
			if (currentCountDown > 0)
			{
				currentCountDown -= Time.deltaTime;
			}
			else
			{
				ResetPowerUp();
				currentCountDown = countDown;
			}
		}

	}

	void Shoot(RaycastHit hit)
	{
		GameObject instantiatedBullet = Instantiate(bullet, nozzle.transform.position, transform.rotation);

		instantiatedBullet.GetComponent<Rigidbody>().velocity = (hit.point - transform.position).normalized * bulletSpeed;

		//instantiatedBullet.GetComponent<Rigidbody>().rotation = Quaternion.LookRotation(instantiatedBullet.GetComponent<Rigidbody>().velocity);
	}

	void TripleShoot(RaycastHit hit)
	{
		//set fire rate higher
		fireRate = poweredFireRate;

		GameObject instantiatedBullet = Instantiate(bullet, nozzle.transform.position, transform.rotation);
		GameObject instantiatedBulletLeft = Instantiate(bullet, nozzle.transform.position + new Vector3(-0.2f, 0, 0), transform.rotation);
		GameObject instantiatedBulletRight = Instantiate(bullet, nozzle.transform.position + new Vector3(0.2f, 0, 0), transform.rotation);

		instantiatedBullet.GetComponent<Rigidbody>().velocity = (hit.point - transform.position).normalized * bulletSpeed;
		instantiatedBulletLeft.GetComponent<Rigidbody>().velocity = (hit.point - transform.position).normalized * bulletSpeed;
		instantiatedBulletRight.GetComponent<Rigidbody>().velocity = (hit.point - transform.position).normalized * bulletSpeed;

		//instantiatedBullet.GetComponent<Rigidbody>().rotation = Quaternion.LookRotation(instantiatedBullet.GetComponent<Rigidbody>().velocity);
		instantiatedBulletLeft.transform.Rotate(new Vector3(0, -25, 0));
		instantiatedBulletRight.transform.Rotate(new Vector3(0, 25, 0));
	}

	void RocketShoot(RaycastHit hit)
	{
		GameObject instantiatedRocket = Instantiate(rocket, nozzle.transform.position, transform.rotation);

		instantiatedRocket.GetComponent<Rigidbody>().velocity = (hit.point - transform.position).normalized * rocketSpeed;

		//instantiatedRocket.GetComponent<Rigidbody>().rotation = Quaternion.LookRotation(instantiatedRocket.GetComponent<Rigidbody>().velocity);
	}

	void ResetPowerUp()
	{
		isPoweredUp = false;
		tripleShoot = false;
		rocketShoot = false;
		singleShoot = true;
		fireRate = defaultFireRate;
	}

}
